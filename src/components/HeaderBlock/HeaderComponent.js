import React from 'react';
import './HeaderComponent.css';

const HeaderComponent = () => {
    return (
        <div className="cover">
            <div className="wrap">
                <h1 className="header">Do something...</h1>
                <p className="description">Use something</p>
            </div>
        </div>
    );
}

export default HeaderComponent;