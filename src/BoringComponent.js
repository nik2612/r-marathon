import './BoringComponent.css';
import React from 'react';

const BoringComponent = (props) => {
    return (
        <div className="boring-display">
            <div className="ui active dimmer">
                <div className="ui text loader">{props.message}</div>
            </div>
        </div>
        
    );
}

BoringComponent.defaultProps = {
    message: "waiting new cool features..."
}

export default BoringComponent;