import React from 'react';
import ReactDOM from 'react-dom';
//import BoringComponent from './BoringComponent';
import HeaderComponent from './components/HeaderBlock/HeaderComponent';

ReactDOM.render(
  <HeaderComponent />,  
  document.getElementById('root')
);